import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';
import { Container, Row, Col, Button, InputGroup, FormControl } from 'react-bootstrap';

function Calculator() {
  const [input, setInput] = useState('');

  const handleClick = (value) => {
    setInput(input + value);
  };

  const handleClear = () => {
    setInput('');
  };

  const handleCalculate = () => {
    try {
      setInput(eval(input).toString());
    } catch (error) {
      setInput('Error');
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleCalculate();
    }
  };

  return (
    <Container className="calculator mt-5">
      <Row>
        <Col xs={12} className="mb-3">
          <InputGroup>
            <FormControl
              placeholder="Enter expression"
              aria-label="Expression"
              aria-describedby="basic-addon2"
              value={input}
              onChange={(e) => setInput(e.target.value)}
              onKeyPress={handleKeyPress}
            />
            <Button variant="outline-secondary" onClick={handleClear}>Clear</Button>
          </InputGroup>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <Button onClick={() => handleClick(7)}>
            7
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick(8)}>
            8
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick(9)}>
            9
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick('+')}>+</Button>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <Button onClick={() => handleClick(4)}>
            4
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick(5)}>
            5
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick(6)}>
            6
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick('-')}>-</Button>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <Button onClick={() => handleClick(1)}>
            1
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick(2)}>
            2
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick(3)}>
            3
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick('*')}>*</Button>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <Button onClick={() => handleClick(0)}>
            0
          </Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick('.')}>
            .
          </Button>
        </Col>
        <Col>
          <Button onClick={handleCalculate}>=</Button>
        </Col>
        <Col>
          <Button onClick={() => handleClick('/')}>/</Button>
        </Col>
      </Row>
    </Container>
  );
}

export default Calculator;
